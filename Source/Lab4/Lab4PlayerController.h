// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Lab4PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class LAB4_API ALab4PlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	ALab4PlayerController();
	void MoveForward(float value);
	void MoveRight(float value);
	void Interact();
	void Jump();

protected: 
	void SetupInputComponent();
};
